
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class GoodsService  {
    
    private static ArrayList<Goods> goodsList = goodsList = new ArrayList<>();
    
   // static {
        //Mock Data
    //    goodsList.add(new Goods("001","lip","Candy", 12.0 , 2 ));
    //    goodsList.add(new Goods("002","Ton","Auna", 15.0 , 3 ));
    //    goodsList.add(new Goods("003","Mask","Hok", 12.0 , 1 ));
    //    goodsList.add(new Goods("004","Tint","Mamo",15.0,1));
   // }

    
    public static boolean addGoods(Goods goods){
        goodsList.add(goods);
        save();
        return true;
    }
    
    public static boolean editGoods(int index,Goods goods){
        goodsList.set(index, goods);
        save();
        return true;
    }
        
    public static boolean delGoods(int index){
        goodsList.remove(index);
        save();
        return true;
    }
    
    public static boolean clearGoods(Goods goods){
        goodsList.clear();
        save();        
        return true;
    }
    
    public static ArrayList<Goods> getGoods() {
        return goodsList;
    }
    
    public static Goods getGoods(int index){
        return goodsList.get(index);
    }
    
    public static boolean delGoodsIndex(int index) {
        goodsList.remove(index);
        save();
        return true;
    }
    
       public static void save(){
            File file = null;
            FileOutputStream fos = null;
            ObjectOutput oos = null;
        try {
            file = new File("dear.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(goodsList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoodsService.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoodsService.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void load(){
            File file = null;
            FileInputStream fis = null;
            ObjectInput ois = null;
        try {
            file = new File("dear.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            goodsList = (ArrayList<Goods>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoodsService.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoodsService.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GoodsService.class.getName()).
                    log(Level.SEVERE, null, ex);
        }  
    }
}
