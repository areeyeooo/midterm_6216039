
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Goods implements Serializable {

    private String ID;
    private String name;
    private String brand;
    private Double price;
    private int amount;
    
    public Goods(String ID,String name,String brand,Double price,int amount){
        this.ID = ID;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;        
    }
    
    public String getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public Double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }
     public void setID(String ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setAmount(int amount) {
        this.amount = amount;
        
    }
    @Override
    public String toString() {
        return ID+"                             "+name+"                           "+brand+"                             "+price+"                            "+amount;
    }
}
